#include <iostream>
#include <array>

#define GLFW_INCLUDE_NONE

#include <GLFW/glfw3.h>
#include <glad/glad.h>

#include <Application.hxx>

constexpr uint16_t windowWidth = 1200;
constexpr uint16_t windowHeight = 800;

static void glfwErrorCallback(int error, const char* description)
{
	std::cout << description << std::endl;
}

static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}

static void glfwFrameBufferSizeCallback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}


int main()
{
	//GLFW
	glfwSetErrorCallback(glfwErrorCallback);

	if (glfwInit() == GLFW_FALSE)
		exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(windowWidth, windowHeight, "Hello World", NULL, NULL);
	if (window == GLFW_FALSE)
	{
		std::cout << "Failed to create a glfw window\n";
		glfwDestroyWindow(window);
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

	glfwSetKeyCallback(window, glfwKeyCallback);


	// GLAD
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == NULL) {
		std::cout << "Failed to initialize OpenGL context\n";
		exit(EXIT_FAILURE);
	}

	std::cout << glGetString(GL_VERSION) << std::endl;
	glfwSwapInterval(1);
	glViewport(0, 0, windowWidth, windowHeight);
	glfwSetFramebufferSizeCallback(window, glfwFrameBufferSizeCallback);



	const char* vertexShaderSource = R"(
        #version 330 core
        layout (location = 0) in vec3 position;

        void main()
        {
            gl_Position = vec4(position.x, position.y, position.z, 1.0);
        }
    )";


	const char* fragmentShaderSource = R"(
        #version 330 core
        out vec4 color;

        void main()
        {
            color = vec4(1.0f, 0.5f, 0.3f, 1.0);
        }
    )";

	uint32_t vertexShader = glCreateShader(GL_VERTEX_SHADER);
	uint32_t fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);

	glCompileShader(vertexShader);

	int result = NULL;
	char message[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &result);

	if (result == NULL)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, message);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << message << std::endl;
	}

	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &result);

	if (result == NULL)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, message);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << message << std::endl;
	}

	uint32_t shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &result);
	if (result == NULL) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, message);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << message << std::endl;
	}

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	std::array<float, 9*2>vertices = {
		-0.5f, -0.5f,  0.0f, // 0
		-0.5f,  0.5f,  0.0f, // 1
		 0.5f,  0.5f,  0.0f, // 2
		 0.5f, -0.5f,  0.0f, // 3
	};

	std::array<uint8_t, 6>indices = {
		0, 1, 3,
		3, 2, 1
	};

	uint32_t VBO = NULL;
	uint32_t EBO = NULL;
	uint32_t VAO = NULL;

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	// Bind
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

	// configfure
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), &indices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);

	// Unbind for safety
	glBindVertexArray(NULL);

	while (glfwWindowShouldClose(window) == GLFW_FALSE)
	{
		glClearColor(0.2f, 0.1f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(shaderProgram);
		glBindVertexArray(VAO);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}


	glDeleteProgram(shaderProgram);

	glfwDestroyWindow(window);
	glfwTerminate();

	exit(EXIT_SUCCESS);
}